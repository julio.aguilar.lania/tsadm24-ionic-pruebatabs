import { Component } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent } from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
  standalone: true,
  imports: [IonHeader, IonToolbar, IonTitle, IonContent, ExploreContainerComponent],
})
export class Tab3Page {
  constructor() {}

  ionViewWillEnter() {
    console.log("TAB3 viewWillEnter");
  }

  ionViewDidEnter() {
    console.log("TAB3 viewDidEnter");
  }

  ionViewWillLeave() {
    console.log("TAB3 viewWillLeave");
  }

  ionViewDidLeave() {
    console.log("TAB3 viewDidLeave");
  }
}
