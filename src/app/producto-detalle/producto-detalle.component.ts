import { Component } from "@angular/core";
import { Producto } from "../productos/producto.model";
import { IonContent, IonLabel, IonHeader, IonTitle, IonGrid, IonRow, IonCol } from "@ionic/angular/standalone";
import { ProductosRemotosService } from "../productos/productos-remotos.service";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector:'app-producto-detalle',
    templateUrl:'producto-detalle.component.html',
    styleUrl:'producto-detalle.component.scss',
    standalone: true,
    imports:[IonContent, IonHeader, IonTitle, IonLabel, IonGrid,IonRow,IonCol],
    providers:[ProductosRemotosService]
})
export class ProductoDetalleComponent{
    producto?: Producto = undefined;

    constructor(private prodService:ProductosRemotosService, private ruta:ActivatedRoute){}

    ionViewWillEnter() {
        let idProducto = -1;
        this.ruta.params.subscribe(params => {
            idProducto = params['id'];
            this.prodService.consultarDetalleProducto(idProducto)
                .subscribe(res => this.producto = res);
        });
    }
}
