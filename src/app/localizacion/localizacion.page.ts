/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Geolocation, Position } from '@capacitor/geolocation';

@Component({
  selector: 'app-localizacion',
  templateUrl: './localizacion.page.html',
  styleUrls: ['./localizacion.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class LocalizacionPage implements OnInit {

  permisoLocation:boolean = false;
  permisoCoarseLocation:boolean = false;
  posicion? : Position;

  constructor() { }

  ngOnInit() {
    Geolocation.checkPermissions().then(ps => {
      this.permisoLocation = (ps.location == 'granted');
      this.permisoCoarseLocation = (ps.coarseLocation == 'granted');
      if (!this.permisoLocation) {
        console.log('pidiendo permiso "location"');
        Geolocation.requestPermissions({permissions:['location']}).then(
          ps => {
            console.log(ps);
            this.permisoLocation = (ps.location == 'granted');
          }
        );
        if (this.permisoLocation) {
          this.obtenerCoordenadas();
        }
      }
      else {
        this.obtenerCoordenadas();
      }
    });
  }

  obtenerCoordenadas() {
    Geolocation.getCurrentPosition().then(pos => this.posicion = pos);
  }


  ionViewWillEnter() {
    console.log("LOCAL viewDidEnter");
  }
  ionViewDidEnter() {
    console.log("LOCAL viewDidEnter");
  }

  ionViewWillLeave() {
    console.log("LOCAL viewWillLeave");
  }

  ionViewDidLeave() {
    console.log("LOCAL viewDidLeave");
  }
}
