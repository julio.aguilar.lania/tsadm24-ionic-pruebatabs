import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, IonCard, IonCardHeader, IonCardContent, IonCardTitle, IonLabel, IonButton, IonIcon } from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { ProductosRemotosService } from '../productos/productos-remotos.service';
import { Producto } from '../productos/producto.model';
import { CommonModule } from '@angular/common';
import { addIcons } from 'ionicons';
import { eye } from 'ionicons/icons';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  imports: [RouterLink,
    IonIcon, IonButton, IonLabel, IonCardTitle, IonCardContent, IonCardHeader, IonCard, CommonModule,
    IonItem, IonList, IonHeader, IonToolbar, IonTitle, IonContent, ExploreContainerComponent],
  providers: [ProductosRemotosService]
})
export class Tab1Page implements OnInit,OnDestroy {

  listaProductos:Producto[] = [];

  constructor(private prodService:ProductosRemotosService) {
    addIcons({'ver':eye});
  }

  ngOnInit():void {
    console.log("TAB1 onInit()");
      // Carga productos una vez
      this.prodService.consultarProductos()
        .subscribe(res => this.listaProductos = res);
  }

  ngOnDestroy(): void {
      console.log("TAB1 onDestroy()");
    }

  ionViewWillEnter() {
    console.log("TAB1 viewWillEnter");
    // Cargaria productos cada vez que se navegue de regreso a la pagina
    //this.prodService.consultarProductos()
    //   .subscribe(res => this.listaProductos = res);
}

  ionViewDidEnter() {
    console.log("TAB1 viewDidEnter");
  }

  ionViewWillLeave() {
    console.log("TAB1 viewWillLeave");
  }

  ionViewDidLeave() {
    console.log("TAB1 viewDidLeave");
  }
}
