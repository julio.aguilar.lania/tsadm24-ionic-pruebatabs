import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Producto } from "./producto.model";
import { Observable } from "rxjs";

@Injectable()
export class ProductosRemotosService {

    static BACKEND_URL = 'http://localhost:3000';

    constructor(private http:HttpClient){}

    consultarProductos() : Observable<Producto[]> {
        console.log('consultarProductos');
        return this.http.get<Producto[]>(ProductosRemotosService.BACKEND_URL + '/productos');
    }

    consultarDetalleProducto(idProducto:number) : Observable<Producto> {
        console.log('consultarDetalleProducto');
        return this.http.get<Producto>(ProductosRemotosService.BACKEND_URL + '/productos/' + idProducto);
    }

    agregarProducto(nuevoProducto: Producto): Observable<Producto> {
        console.log('agregarProducto(' + nuevoProducto.nombre + ')');
        /* EJEMPLO DE CODIGO QUE AJUSTA IDs
        let conteoProductos : number = 0;
        this.http.get<{count:number}>(ProductosRemotosService.BACKEND_URL + '/productos/count')
            .subscribe(res => { 
                conteoProductos = res.count;
            })
            */
        let productoBackend = {
            idProducto: 5,
            nombre: nuevoProducto.nombre,
            fechaIngreso: nuevoProducto.fechaIngreso + 'T00:00:00.000Z',
            tipoProducto: nuevoProducto.tipoProducto.tipo,
            descripcion: ''
        };
        if (nuevoProducto.descripcion) {
            productoBackend.descripcion = nuevoProducto.descripcion;
        }
        return this.http.post<Producto>(ProductosRemotosService.BACKEND_URL + '/productos', productoBackend);
        
    }

    // ProductoToProductoBackend(prod: Producto) : ProductoBackend {}
    // ProductoBackendToProducto(pb: ProductoBackend) : Producto {}

    static valorComplejo : Producto;
    setValorComplejo(p:Producto) { ProductosRemotosService.valorComplejo = p;}
    getValorComplejo():Producto {return ProductosRemotosService.valorComplejo;}
}