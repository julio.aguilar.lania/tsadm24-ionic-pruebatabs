import { Component } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonCard, IonCardTitle, IonCardHeader, IonGrid, IonRow, IonCol } from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { BatteryInfo, Device, DeviceInfo } from '@capacitor/device';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  standalone: true,
  imports: [CommonModule,
    IonCol, IonRow, IonGrid, IonCardHeader, IonCardTitle, IonCard, IonHeader, IonToolbar, IonTitle, IonContent, ExploreContainerComponent]
})
export class Tab2Page {

  infoDispositivo?:DeviceInfo;
  infoBateria?:BatteryInfo;
  constructor() {}

  ionViewWillEnter() {
    console.log("TAB2 viewWillEnter");
    Device.getInfo().then(info => {
      console.log(info);
      this.infoDispositivo = info;
    });
    Device.getBatteryInfo().then(info => {
      this.infoBateria = info
      if (info.batteryLevel) {
        info.batteryLevel = info.batteryLevel * 100;
      }
    });
  }

  ionViewDidEnter() {
    console.log("TAB2 viewDidEnter");
  }

  ionViewWillLeave() {
    console.log("TAB2 viewWillLeave");
  }

  ionViewDidLeave() {
    console.log("TAB2 viewDidLeave");
  }
}
